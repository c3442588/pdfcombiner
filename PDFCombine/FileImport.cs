﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDFCombine
{
    static class FileImport
    {
        static OpenFileDialog fd = new OpenFileDialog();
        public static String importPDF() {
            fd.Filter = "Pdf Files|*.pdf";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                Console.WriteLine(fd.FileName);
                return fd.FileName;
            } else
            {
                return null;
            }
        }        
    }
}
