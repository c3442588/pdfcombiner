﻿using Spire.Pdf;
using Spire.Pdf.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFCombine
{
    static class Combine
    {
        public static PdfDocument MergePDF(string inputPath1, string inputPath2)
        {
            PdfDocument pdf1 = new PdfDocument();
            PdfDocument pdf2 = new PdfDocument();
            pdf1.LoadFromFile(inputPath1);
            pdf2.LoadFromFile(inputPath2);

            PdfPageBase page = pdf1.Pages[0];
            SizeF size = page.Size;
            int index = 1;
            for (int i = 0; i < pdf2.Pages.Count; i++)
            {
                page = pdf1.Pages.Insert(index, size, new PdfMargins(0));
                page.Canvas.DrawTemplate(pdf2.Pages[i].CreateTemplate(), new PointF(0, 0));
                index += 2;
            }
            //pdf1.SaveToFile("output.pdf");
            return pdf1;
        }

    }
}
