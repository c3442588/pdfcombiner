﻿using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDFCombine
{
    public partial class MainForm : Form
    {
        String PDF1,PDF2;
        PdfDocument CPDF = new PdfDocument();
        SaveFileDialog sd = new SaveFileDialog();

        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonPDF2_Click(object sender, EventArgs e)
        {
            PDF2 = FileImport.importPDF();            
            textBoxPDF2.Text = PDF2;
        }

        private void buttonCombine_Click(object sender, EventArgs e)
        {
            try
            {
                CPDF = Combine.MergePDF(PDF1, PDF2);
                sd.Filter = "Pdf Files|*.pdf";
                if (sd.ShowDialog() == DialogResult.OK)
                {                    
                    CPDF.SaveToFile(sd.FileName);
                    Process.Start(sd.FileName);
                }
            }
            catch (ArgumentNullException ane)
            {
                MessageBox.Show("Make sure you've selected the PDFs you want to combine!", "Alert", MessageBoxButtons.OK);
            }
        }

        private void buttonPDF1_Click(object sender, EventArgs e)
        {
            PDF1 = FileImport.importPDF();
            textBoxPDF1.Text = PDF1;
        }        
    }
}
